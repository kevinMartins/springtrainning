package com.treinamentospring.loja.model;

public enum TipoPreco {
	 EBOOK, IMPRESSO, COMBO;
}
